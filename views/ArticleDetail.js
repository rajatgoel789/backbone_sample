window.ArticleDetail = Backbone.View.extend({
  
  id: "article-detail",

  initialize: function () {
  	console.log('ArticleDetail initialized');
  },

  events:{
     'click dt a.article-remove'   : 'deleteArticle',
     'click  .client-selected-post': 'showSelectedClientPost',
     'click  .tile_like'           : 'userTileDisLike',         
     'click  .tile_dislike'        : 'userTileDisLike',
     'click  .post-share'        : 'OpenShareArticleModal'
  },

  render: function () {
    $(this.el).html(this.template());
    return this;  
  },
   
   showSelectedClientPost: function (e) {    
	   var class_check = $(e.currentTarget).attr('class');
	   var clientModel = new Client();
	   var postId = $(e.currentTarget).attr('data-postid');
	   clientModel.set({selected_post : "checked"});
	    if(clientModel.get("selected_post") == "checked"){
	     var selected_post = clientModel.get('posts');
	     
	       if($.trim(class_check) == "client-selected-post" && $.cookie('post-cookie') == undefined){
	        $(e.currentTarget).addClass("like-active");
	        selected_post.push(parseInt(postId));
	        clientModel.set({article_like : "like-active",id : parseInt(postId) });
             var clientview = new ClientView({model: clientModel});

	     }else if($.trim(class_check) == "client-selected-post" && $.cookie('post-cookie') != undefined){

	       var selected_post = $.parseJSON($.cookie('post-cookie'));
	        for(var i = 0; i < selected_post.length; i++)  {
	          selected_post[i] = parseInt(selected_post[i], 10);
	        }
	        $(e.currentTarget).addClass("like-active");
	        selected_post.push(parseInt(postId));
	        clientModel.set({article_like : "like-active",id : parseInt(postId) });
             var clientview = new ClientView({model: clientModel});
	     }else {
	       var selected_post = $.parseJSON($.cookie('post-cookie'));
	        for(var i = 0; i < selected_post.length; i++)  {
	          selected_post[i] = parseInt(selected_post[i], 10);
	        }
	        selected_post.splice( $.inArray(parseInt(postId), selected_post), 1 );
	        $(e.currentTarget).removeClass("like-active");
	        clientModel.set({article_like : "",id : parseInt(postId) });
             var clientview = new ClientView({model: clientModel});
	     }
	      $.cookie('post-cookie', JSON.stringify(selected_post));
	   }
   },
      OpenShareArticleModal: function (e) {//alert("client");
	 // alert("sa");
      $(".article-share").remove();
      $("#product_model").modal('hide');
      $(".modal-backdrop.in").css("opacity", 0);
     // var tiles = new Tiles();
      var tileid= $(e.currentTarget).attr('data-postid'); 
       $.ajax({
          url      :  '../article_detail/'+tileid,
         // contentType : "application/json; charset=utf-8",
          dataType    : 'json',
         success: function (response) {
          //alert("dsds");
	         { //alert(tileid); 
      console.log("Complete Response",response);
      //var tileModel= response.where({'id':parseInt(tileid)  });
      articleShareView = new ArticleShareView({ collection: response});
     // alert("Hiii");
      articleShareView.render();//alert("After Render");
      $("body").append(articleShareView.el);
      $("#share_article").modal('show');
        }
		           },
          error: function (error) {
          console.log("Data not fetched.");
         }
      });
      
        
	//alert('ok working');
    /*  $(".article-share").remove();
      $("#product_model").modal('hide');
      $(".modal-backdrop.in").css("opacity", 0);
      var tiles = new Tiles();

      tiles.fetch({
        success: function(response) { //alert(tileid); 
      //console.log("Complete Response",response);
      var tileid= $(e.currentTarget).attr('data-postid');alert(tileid); //alert('Tile modal is',tileModel); 
    //  var tileModel= response.where({'id':parseInt(tileid)  }); //console.log('Tile is',tiles);
      articleShareView = new ArticleShareView({model: tileModel, collection: tiles}); //console.log(articleShareView);
      articleShareView.render();
      $("body").append(articleShareView.el);
      $("#share_article").modal('show');
        },
        error: function() {
           console.log("Data not fetched.");
        }
      });*/
   },
   
  	userTileDisLike: function (e) {
	   var tileid = $(e.currentTarget).attr('data-tileid'); //alert(tileid);
	   var currentclass = $(e.currentTarget).attr('class');   //alert(currentclass) ;
	   if (currentclass.toLowerCase().indexOf("tile_like") >= 0) {
		 var tl = 1 ;
		 var tdl=0 ;
		}else if(currentclass.toLowerCase().indexOf("tile_dislike") >= 0){
		   var tl = 0 ;
		   var tdl=1 ;
		}else {
			var tl = 0 ;
			var tdl=0 ;
		}
	   var tileDisLike = new Client({
		  tile_id: tileid,
		  tile_like: tl,
		  tile_dislike: tdl,
	   });
       
       tileDisLike.save({},{
		 success :  function(model,response,xhr){	
			if(response.error_msg) {
			  alert(response.error_msg);
            } else {
			    if (response.like == 1) {			  
			       tileDisLike.set({tile_like :1,tile_dislike :0})
			       var tile_like_status =tileDisLike.get('tile_like') ;
			       
			        if (tile_like_status==1) {
						$(e.currentTarget).css('pointer-events', 'none');
						$(e.currentTarget).parent().prev().find("a").css('pointer-events', 'auto')
						console.log($(e.currentTarget).parent().prev().find("a"));  
		         		if ($(e.currentTarget).parent().prev().find("a").hasClass('dislike-active')) {
							$(e.currentTarget).parent().prev().find("a").removeClass("dislike-active");
						 }
						if($(e.currentTarget).hasClass('like-active')){
							
					    }else {
						  $(e.currentTarget).addClass("like-active");
						}
			        }
			    
			    }else if(response.dislike == 1) {
				
				   tileDisLike.set({tile_like :0,tile_dislike :1}) ;
			       var tile_like_status =tileDisLike.get('tile_like') ;
			       if (tile_like_status==0) {
				
					$(e.currentTarget).css('pointer-events', 'none');
					$(e.currentTarget).parent().next().find("a").css('pointer-events', 'auto');
					 if ($(e.currentTarget).parent().next().find("a").hasClass('like-active')) {
						$(e.currentTarget).parent().next().find("a").removeClass("like-active");
						
					 }
					
					 if($(e.currentTarget).hasClass('dislike-active')){
					  }
					  else {
						  $(e.currentTarget).addClass("dislike-active");
					   }
				
			        }
				}
			
			}
		},
		error: function(model,response,xhr){
		  alert("error");
		}
	  });
   },
   deleteArticle :function (e)
   { var alphaval=$("#alpha").val();
	 var r=confirm("Are you sure to delete this content ");
         if (r==true){ }else{ return false ; }  
   
   
   
     $('.client').remove();
  //alert("Call");
  //var clientview=new ClientView();
  //clientview.deleteClientArticle(e);
    var articleId = $(e.currentTarget).attr('data-tileid');
    var clientId = $(e.currentTarget).attr('data-cid');//alert(clientId); alert(articleId);
    var client = new Client({ id: articleId, isdelete: 1,req_type:'client',client_id:clientId });
    var clients = new Clients([], { alpha: alphaval });
    
    var that = this;
       client.destroy({
       data: JSON.stringify(client),
       contentType: 'application/json',
       dataType: "text",
       success: function(response) {
         clients.fetch({
          success: function(response) {
	    console.log(response.toJSON(), "clientsCollection");
            var clientview = new ClientView();
            $(clientview.el).html(clientview.template({clientsCollection: response.toJSON() }));           
	    $(".col-lg-2").after(clientview.el);
           }
         });
	$( '#product_model' ).remove();
$( '.modal-backdrop' ).remove();
$( 'body' ).removeClass( "modal-open" ); 
	//$('#product_model').modal('hide') ;
        //$(".modal-backdrop.in").css("opacity", "0"); 
       },
       error: function(error) {
         console.log('error', error);
       }
   });
 
  
  

    
   }


});