window.UserPlusUrlModalView = Backbone.View.extend({
	
  id: "user-url",
  
  initialize: function () {
  	console.log('UserPlusUrlModalView initialized');
  },

  render: function () {
    $(this.el).html(this.template());
    return this;
  }

});