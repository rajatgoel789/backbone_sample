window.ClientSubView = Backbone.View.extend({
  
  className: "client-sub",
  events: {

    'focus .client-search' : 'clientSearch',
    'click .ui-autocomplete li a' :'specificClientSearch'
  },
  initialize: function () {
  	console.log('ClientSubView initialized');
    $.ajax({
      type: "GET",
      url: "../email_date",
      dataType: "json",
      success: function( data ) {  
      $('.email_sdbar').find('.date').html(data.nextdate)
      $('.email_sdbar').find('.time').html(data.nextTime)
      if(data.maildelivery == 1){
        $('.thumb_list2').find('.auto-delivery-yes').addClass('like-active');
        $('.thumb_list2').find('.auto-delivery-no').removeClass('dislike-active');
      }else{
        $('.thumb_list2').find('.auto-delivery-no').addClass('dislike-active');
        $('.thumb_list2').find('.auto-delivery-yes').removeClass('like-active');
      }
        
      },
      error :function(a,b,c){  
        console.log(a,b,c); }
    });
},
  clientSearch :function()
  {
  var that=this ;

        $('.client-search').autocomplete({ 

   source: function( request, response ) {
				$.ajax({
					url: "../search_clients",
					dataType: "json",
					data: {
						featureClass: "P",
						style: "full",
						maxRows: 12,
						user: request.term
					},
					success: function( data ) {  console.log("sds", data); //alert("");
						response( $.map( data, function( item ) {
							if(data.error){
                return {
                  label: data.error
                }
              }

              return {
								label: item.client_name ,
								value: item.client_id ,
								customlabel: item.client_lastname 
							}
						}));
					},
					error :function(a,b,c){  console.log(a,b,c); }
				});
			},
			 select: function(event, ui) { //alert(ui.item.value) ; console.log(ui);
           $('#client_id_search').val(ui.item.value);
           if(ui.item.value !='undefined' && typeof ui.item.value != undefined && ui.item.value != 'No match Found')
           {
           	that.specificClientSearch(ui.item.value);
           }	
        // $('.client-search').val(ui.item.customlabel);
        return false;
    },  messages: {
        noResults: '',
        results: function() {}
    }

        });

  },

  specificClientSearch :function(clientid){
     //alert(clientid);
  		var that=this ;
     //var filter = $(e.currentTarget).text();
      var client = new Client();
     //  client.set({'filter': filter}) ;     
  client.save({},{ 
      success:function(model,response,xhr){ //alert(""); 
      //alert("");
       console.log("SearchResponse",response);
    var clientView = new ClientView();
    $(".client").html('');
  //console.log("HTML",clientView.template({clientsCollection: response }));
   $(clientView.el).html(clientView.template({clientsCollection: response }));

   $(clientView.el).insertAfter('.col-lg-2');
   console.log("final",response);
      },
      error: function(model,response,xhr){
      alert("error");
      },
      url:'../client_filter/?client='+clientid
    });
},
  render:function () {
    $(this.el).html(this.template());
    return this;
  }
});
