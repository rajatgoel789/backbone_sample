window.SmartDeliveryView  = Backbone.View.extend({
 
  className: 'smart-delivery', 

  initialize: function() {
    console.log('SmartDeliveryView initialized');

  },


  events: {
    'click  #smart_update': 'edit_delivery_time',
  }, 


  edit_delivery_time:function(){

    var smart_type = $('#smart_type').val();
  
        var SmartType = '';

         if(smart_type != 'undefined')
         {
            SmartType = smart_type ;
            console.log("SmartType",SmartType);
         }
       
       $.cookie('SmartType',SmartType);


    
      var smart_frequency = $('#smart_frequency').val();
   
        var SmartFrequency = '';

         if(smart_frequency != 'undefined')
         {
            SmartFrequency = smart_frequency ;
            console.log("SmartFrequency",SmartFrequency);
         }
       
       $.cookie('SmartFrequency',SmartFrequency);

    
    var smart_date = $('#smart_date').val();

        var SmartDate = '';

         if(smart_date != 'undefined')
         {
            SmartDate = smart_date ;
            console.log("SmartDate",SmartDate);
         }
       
       $.cookie('SmartDate',SmartDate);

  
  var smart_hours = $('#smart_hours').val();
  
        var SmartHours = '';

         if(smart_hours != 'undefined')
         {
            SmartHours = smart_hours ;
            console.log("SmartHours",SmartHours);
         }
       
       $.cookie('SmartHours',SmartHours);


  var smart_minutes = $('#smart_minutes').val();
  
        var SmartMinutes = '';

         if(smart_minutes != 'undefined')
         {
            SmartMinutes = smart_minutes ;
            console.log("SmartMinutes",SmartMinutes);
         }
       
       $.cookie('SmartMinutes',SmartMinutes);


  var smart_time = $('#smart_time').val();
  
        var SmartTime = '';

         if(smart_time != 'undefined')
         {
            SmartTime = smart_time ;
            console.log("SmartTime",SmartTime);
         }
       
       $.cookie('SmartTime',SmartTime);
       
       $("#del_tme_update").modal('hide');

       $(".review-email").remove();
       $( '.modal-backdrop' ).remove();
       $( 'body' ).removeClass( "modal-open" );

    var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);

  },



  render:function () {
    
    var that = this;
     $.ajax({
          url      :'../clients_emails/',
          type     : 'POST',
          contentType : "application/json; charset=utf-8",
          dataType    : 'json',
         success: function (response) {
         

            var delivery_date = res[0].smart_data[0].delivery_date;
            
            if(delivery_date != '' && delivery_date != '30\/11\/-0001' ) {
            $.cookie('smartdatabase_delivery_date',delivery_date);
            
            }else {
              $.cookie('smartdatabase_delivery_date','');
    
             }
          

           if($.cookie('smartdatabase_delivery_date')) {
            if($.cookie('smartdatabase_delivery_date') != 'undefined') {
             var new_date = $.cookie('smartdatabase_delivery_date');
         
             $.cookie('SmartDate',new_date);
        
             console.log("cookie value" ,$.cookie('SmartDate')) ;
         
           }
         }


            var delivery_hours = res[0].smart_data[0].delivery_hours;

            if(delivery_hours !='') {
            $.cookie('smartdatabase_delivery_hours',delivery_hours);
            
            }else {
              $.cookie('smartdatabase_delivery_hours','');
             }

           
           if($.cookie('smartdatabase_delivery_hours')) {
            if($.cookie('smartdatabase_delivery_hours') != 'undefined') {
             var new_hours = $.cookie('smartdatabase_delivery_hours');
         
             $.cookie('SmartHours',new_hours);
        
             console.log("cookie value" ,$.cookie('SmartHours')) ;
         
           }
         }
          


            var delivery_minutes = res[0].smart_data[0].delivery_minutes;

             if(delivery_minutes !='' ) {
            $.cookie('smartdatabase_delivery_minutes',delivery_minutes);
            
            }else {
              $.cookie('smartdatabase_delivery_minutes','');
             }
           
            if($.cookie('smartdatabase_delivery_minutes')) {
            if($.cookie('smartdatabase_delivery_minutes') != 'undefined') {
             var new_minutes = $.cookie('smartdatabase_delivery_minutes');
         
             $.cookie('SmartMinutes',new_minutes);
        
             console.log("cookie value" ,$.cookie('SmartMinutes')) ;
         
           }
         }
               


            var delivery_time = res[0].smart_data[0].delivery_time;
            if(delivery_time !='' ) {
            $.cookie('smartdatabase_delivery_time',delivery_time);
            
            }else {
              $.cookie('smartdatabase_delivery_time','');
             }

           if($.cookie('smartdatabase_delivery_time')) {
            if($.cookie('smartdatabase_delivery_time') != 'undefined') {
             var new_time = $.cookie('smartdatabase_delivery_time');
         
             $.cookie('SmartTime',new_time);
        
             console.log("cookie value" ,$.cookie('SmartTime')) ;
         
           }
         }


            var type = res[0].smart_data[0].type;

             if(type !='' ) {
            $.cookie('smartdatabase_type',type);
            
            }else {
              $.cookie('smartdatabase_type','');
             }

           if($.cookie('smartdatabase_type')) {
            if($.cookie('smartdatabase_type') != 'undefined') {
             var new_type = $.cookie('smartdatabase_type');
         
             $.cookie('SmartType',new_type);
        
             console.log("cookie value" ,$.cookie('SmartType')) ;
         
           }
         }


            var frequency = res[0].smart_data[0].frequency;
          
             if(frequency !='' ) {
            $.cookie('smartdatabase_frequency',frequency);
            
            }else {
              $.cookie('smartdatabase_frequency','');
             }

           if($.cookie('smartdatabase_frequency')) {
            if($.cookie('smartdatabase_frequency') != 'undefined') {
             var new_frequency = $.cookie('smartdatabase_frequency');
         
             $.cookie('SmartFrequency',new_frequency);
        
             console.log("cookie value" ,$.cookie('SmartFrequency')) ;
         
             }
           }

				 $(that.el).html(that.template());
				 $("body").append(that.el);
         $("#del_tme_update").modal("show");  

          },

          error: function (error) {
            console.log("failed");
          
         }
      });


    
    
    }
 
});
