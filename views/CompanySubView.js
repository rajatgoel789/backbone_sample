window.CompanySubView = Backbone.View.extend({
 
  className: 'company-sub', 
 
  events: {
    'click  .list-group a'  : 'categorySelected'
   },

	initialize: function() {
	  console.log('CompanySubView initialized');
	},
	render:function () {
	    var that = this;
	    var categories = new Categories();

	    categories.fetch({
	      success: function(response) {
	        $(that.el).html(that.template({categoriesCollection: response.toJSON()}));
	        return that;
	      },
	      error: function() {
	        console.log("Data not fetched.");
	      }
	    });
	},

	categorySelected: function(e) {
		$(".email-contents").remove();
		$(".contacts-groups").remove();
		$(".review-email").remove();
		
		var $selected = $(e.currentTarget).attr('data-categoryid');
		$(e.currentTarget).siblings().removeClass( "cat-selected" );
		//$(e.currentTarget).siblings.removeClass('cat-selected');
		$(e.currentTarget).addClass('cat-selected');
        if($selected == 'all') {
	    $("#loaderdiv").show();

          $(".company").remove();

          var companyview = new CompanyView();
          companyview.render();
          $('.col-lg-2').after(companyview.el);

          setTimeout(companyview.loadImages, 2000);
        }
        else {
        	var that = this;
        	$.ajax({
			  
			  url: company_filter+"?category_id="+$selected,
			  success: function (data) {
				 $(".company").remove();
				 $("#tiles").html('');
				 console.log(data);
				  var companyview = new CompanyView({ dinesh : $selected });
				  $("#loaderdiv").show();
	   	      $(companyview.el).html(companyview.template({tilesCollection: $.parseJSON(data)}));
	              $('.col-lg-2').after(companyview.el);
	              setTimeout(companyview.loadImages, 2000);
			  }
           });

        }
    }

});
