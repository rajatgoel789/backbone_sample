window.ClientView = Backbone.View.extend({
  
  className: "client",

  initialize: function (options) {
    console.log('ClientView initialized');
    if(this.model){
      console.log("this.model", this.model);
      var modelid=this.model.get('id');
      if(this.model.get('article_like')  == 'like-active') {
         this.model.get('posts').push(modelid);
         $(".col-lg-9").find("[data-postid='" + modelid + "']").addClass(this.model.get('article_like'));
      } else {
         this.model.get('posts').splice( $.inArray(modelid, this.model.get('posts')), 1 );
         $(".col-lg-9").find("[data-postid='" + modelid + "']").removeClass('like-active');
      }
    }  
  },

  events: {
    'click ul.pagination li a'    : 'filterArticles',
  //  'click .client-article a img' : 'showArticleDetails',
    'click .client-post-detail'   : 'clientPostDetail',
    'click .client-selected-post' : 'showSelectedClientPost',
    'click .post-remove'          : 'deleteClientArticle'
  },
 
  filterArticles: function(e) {
    var that=this ;
    var filter = $(e.currentTarget).text();
    console.log(filter,"that data",that);
    var client = new Client();
    client.set({'filter': filter}) ;     
    client.save({},{
      success:function(model,response,xhr){ 
        console.log("FA",response,"that.el",that.el,"Find Div");
     //$(".client").html('');
     // $(".top_cont").find(".container").find(".row").find(".col-lg-2").after('<div class="client">'+that.template({clientsCollection: response })+'</div>')
     // $(".top_cont").find(".container").find(".row").find(".col-lg-2").after(that.template({clientsCollection: response }));
        $(that.el).html(that.template({clientsCollection: response }));
        $("#alpha").val(filter);
        return that;
          

        console.log(response);
      },
      error: function(model,response,xhr){
        alert("error");
      },
      url:'../client_filter/?user='+filter
    });

  } ,

  render:function () {  $(".client").remove();
    var that = this;
    var clients = new Clients([], { alpha: ''});
    clients.fetch({
      success: function(response) {
          that.remove_extra_views();
        $(that.el).html(that.template({clientsCollection: response.toJSON() }));
        $("#alpha").val(response.toJSON()[0].active_alphabet.toUpperCase());
        $("#loaderdiv").hide();
      
        return that;

      },
      error: function() {alert("error");   $("#loaderdiv").hide();
        console.log("Data not fetched.");
      }
    });

  },

  remove_extra_views: function() {
    
    
      $(".company").remove();
     $(".company-sub").remove();
    
    // $(".client").remove();
    //$(".client-sub").remove();
   
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();
   

  },
  showArticleDetails: function(e) {
    var a = $(e.currentTarget).attr('src');
    console.log(a);
    $("#client-article").remove();
    this.clientArticleDetail = new ClientArticleDetail();
    this.clientArticleDetail.render();
    $("body").append(this.clientArticleDetail.el);
  },

  clientPostDetail: function (e) { //alert("call");
    e.preventDefault();
    $("#article-detail").remove();
    alphabet = $("#alpha").val();
    var clients = new Clients([], { alpha: alphabet});
    clients.fetch({
      success: function(response) {
        console.log("Client Post Response",response.toJSON());
        var tileid = $(e.currentTarget).attr('data-tileid');
        dine = response.toJSON();
        console.log("GO GOA GONE12", dine);
        var customArray = {};
        $.each(dine[0].data, function( index, value ) {
          var clientid=value.cid;
          $.each(value.client_data, function( ix, data) {
            if(data.id == tileid){
              customArray['id'] = data.id;
              customArray['title'] = data.title;
              customArray['image'] = data.image;
              customArray['url'] = data.url;
              customArray['tile_like'] = data.tile_like;
              customArray['tile_dislike'] = data.tile_dislike;
              customArray['clientid'] = clientid;
               customArray['source'] = data.source;
                 customArray['first_words'] = data.first_words;
               
            }
          });
        });
       console.log("customArray", customArray);
       articleDetail = new ArticleDetail();
       $(articleDetail.el).html(articleDetail.template({tile_model: customArray}));
       $("body").append(articleDetail.el);
       $("#product_model").modal("show");  
      },
      error: function() {
        console.log("Data not fetched.");
      }
    });
  },

 showSelectedClientPost: function (e) {  
   var class_check = $(e.currentTarget).attr('class');
   var clientModel = new Client();
   var postId = $(e.currentTarget).attr('data-postid');
   clientModel.set({selected_post : "checked"});
    if(clientModel.get("selected_post") == "checked"){
     var selected_post = clientModel.get('posts');
     
       if($.trim(class_check) == "client-selected-post" && $.cookie('post-cookie') == undefined){
        $(e.currentTarget).addClass("like-active");
        selected_post.push(parseInt(postId));
 

     }else if($.trim(class_check) == "client-selected-post" && $.cookie('post-cookie') != undefined){

       var selected_post = $.parseJSON($.cookie('post-cookie'));
        for(var i = 0; i < selected_post.length; i++)  {
          selected_post[i] = parseInt(selected_post[i], 10);
        }
        $(e.currentTarget).addClass("like-active");
        selected_post.push(parseInt(postId));
     }else {
       var selected_post = $.parseJSON($.cookie('post-cookie'));
        for(var i = 0; i < selected_post.length; i++)  {
          selected_post[i] = parseInt(selected_post[i], 10);
        }
        selected_post.splice( $.inArray(parseInt(postId), selected_post), 1 );
        $(e.currentTarget).removeClass("like-active");
     }
      $.cookie('post-cookie', JSON.stringify(selected_post));
   }
 },

 deleteClientArticle: function (e) {
   
    var r=confirm("Are you sure to delete this content ");
    if (r==true){ }else{ return false ; }
    var articleId = $(e.currentTarget).attr('data-tileid');
    var clientId = $(e.currentTarget).attr('data-cid');
    var client = new Client({ id: articleId, isdelete: 1,req_type:'client',client_id:clientId });
    var clients = new Clients([], { alpha: $("#alpha").val()});
    var that = this;
       client.destroy({
       data: JSON.stringify(client),
       contentType: 'application/json',
       dataType: "text",
       success: function(response) {
         ///alert('success');
         $(".client").remove();
         $("#loaderdiv").show();
         var clientview = new ClientView();
         clients.fetch({
          success: function(response) {//console.log("clientview.el",clientview.el) ; alert("HiII");
            $(clientview.el).html(clientview.template({clientsCollection: response.toJSON() }));
            $(".col-lg-2").after(clientview.el);
            $("#loaderdiv").hide(); 
           }

         });
       },
       error: function(error) { 
         console.log('error', error);   $("#loaderdiv").hide(); 
       }
   });
 } ,
 afterRender: function() { 
    console.log('afterRender'); 
  } 

});