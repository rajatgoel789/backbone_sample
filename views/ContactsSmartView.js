(function() {
    var flag_edit = "";
window.ContactsSmartView  = Backbone.View.extend({
 
	className: 'contacts-smart', 

	initialize: function(options) {
	  console.log('ContactsSmartView initialized', options);
    if(typeof options != 'undefined') {
      
    flag_edit ="set";
    this.options = options;
    this.flag =  this.options.flag;
    console.log("this.flag",this.flag);
    }
	},

   events: {
    'click  #email': 'email',
    'click  #review': 'review',
    'click  #next': 'next',
    'click .css-checkbox':'checked_groups',
    'click #all':'check_all'
	},

	email: function () {

  //console.log ("length",$.cookie('smartclients').length);

   //if( $.cookie('smartclients') === null  || $.cookie('smartclients') =='undefined' || $.cookie('smartclients').length == 2)
   if(   $("#smartclients").val() === null  ||   $("#smartclients").val() =='undefined' ||   $("#smartclients").val().length == 2)

     {
       // console.log ("length",$.cookie('smartclients').length);
        $(".check-error").html("Please select at least 1 Client to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }

  else {

      $.ajax({
        url: "../clients_emails",
        dataType: "json",
        success: function(result) {
          res=result;

          // console.log("harjeet",result);
           
		    $(".contacts-smart").remove();
        $(".review-email").remove();
		    var view = new EmailContentsView();
        view.render('',res);
        $('.col-lg-2').after(view.el);
      },

       error :function(result){ 
         console.log("error",result);
          }
      });

    }

	},

  review: function () {

      if( $.cookie('TempId') == null  || $.cookie('TempId') == 0 || $.cookie('TempId') == '')
     {
        $(".check-error").html("Please check an Email Template selected by you.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
          return false ;
           //if( $.cookie('smartclients') === null  || $.cookie('smartclients') =='undefined' || $.cookie('smartclients').length == 2)
              if(   $("#smartclients").val() === null  ||   $("#smartclients").val() =='undefined' ||   $("#smartclients").val().length == 2)
                {
                $(".check-error").html("Please select at least 1 Client to proceed further.");
                  $(".check-error").fadeIn().css("display","block");
                  $(".check-error").delay(2000).slideUp("slow"); 
              }

     }else  if(   $("#smartclients").val() === null  ||   $("#smartclients").val() =='undefined' ||   $("#smartclients").val().length == 2)
     {
       // console.log ("length",$.cookie('smartclients').length);
        $(".check-error").html("Please select at least 1 Client to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
          return false ;
     }

     else {
      
   $(".contacts-smart").remove();
    var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);
      }
  },
  
  next: function () {
   if(   $("#smartclients").val() === null  ||   $("#smartclients").val() =='undefined' ||   $("#smartclients").val().length == 2)   {
        $(".check-error").html("Please select at least 1 Client to proceed further.");
           $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }
  else {
    
    $.ajax({
        url: "../clients_emails",
        dataType: "json",
        success: function(result) {
          res=result;

           //console.log("harjeet",result);
           
        $(".contacts-smart").remove();
        $(".review-email").remove();
        var view = new EmailContentsView();
        view.render('',res);
        $('.col-lg-2').after(view.el);
      },

       error :function(result){ 
         console.log("error",result);
          }
      });
      }
  },

   checked_groups: function (e) {
   
    var group_unchecked = $(e.currentTarget).attr('data-clientid');
    var next = $(e.currentTarget).next();
    var cls = $(e.currentTarget).attr('class');
       var a = $("#smartclients").val() ;
    
          var SmartArray = [];
          var SmartArray =  $.parseJSON(a);

         for(var i = 0; i < SmartArray.length; i++) 
        {
           SmartArray[i] = parseInt(SmartArray[i],10);
        } 

          console.log("AllClients", SmartArray);


   if(($.inArray(parseInt(group_unchecked), SmartArray) >= 0) && $(e.currentTarget).hasClass('css-checkbox'))  {

       $(e.currentTarget).removeClass("check");
        next.removeClass("check");

       $(e.currentTarget).addClass("smart_uncheck");
          next.addClass("smart_uncheck");

  
        $.each( SmartArray, function( index, value ) {

        if(group_unchecked ==  value)
        {
          removeindex = index;
        }

                  });

          SmartArray.splice(removeindex, 1 );
        }

    
    else {  

       $(e.currentTarget).addClass('check');
       next.addClass('check');
       $(e.currentTarget).removeClass("smart_uncheck");
        next.removeClass("smart_uncheck");

       SmartArray.push(parseInt(group_unchecked));
      
       }

       console.log("SmartArray", SmartArray);
        $("#smartclients").val( JSON.stringify(SmartArray))  ;
       // $.cookie('smartclients', JSON.stringify(SmartArray));

   },

   	check_all:function(e){
		$('#loaderdiv').show();
		var that = this;
		 var emailClients = new EmailClients([], { view: contacts});
		 
     
		  emailClients.fetch({
		  success: function(response) { //console.log('all response is ',response);
		// console.log('response is ',response);
		var SmartArray = [];
		var smartview = response.toJSON();
	        var len = smartview[0].all_clients.length; //alert(len);
	       //console.log('json is ', smartview);
	      
	      var clas = $(e.currentTarget).attr('class'); //alert(clas);
	      
	      if($(e.currentTarget).hasClass('check')){
	       //alert('if class');
	       ($(e.currentTarget)).removeClass('check');
	       $('.css-label').removeClass('check');
	       var a =  $("#smartclients").val() ;// $.cookie('UserArray') ;
		   var SmartArray = [];
		   $("#smartclients").val(JSON.stringify(SmartArray)) ;
		$('#loaderdiv').hide();
	 
	      }else{ //alert('else className');
	       ($(e.currentTarget)).addClass('check');
	       $('.css-label').addClass('check');
	 $.each([smartview[0].all_clients],function(index,value){
	   //alert('ok');console.log(value);
	   for(var k=0;k<len;k++){ //alert(k);
			//console.log('c id is '+value[k]['client_id']);
			 //console.log('client id is ',smartview[k]['client_id']);
			 SmartArray[k] = JSON.parse(value[k]['client_id']);
			 
		     }
	    $('#smartclients').val(JSON.stringify(SmartArray));
	    $('#loaderdiv').hide();
	 });
		   
	 
	      }
	 
	      }
	     });
	   },
	
	render:function () {
   //console.log("harry",res);
      contacts = $.cookie('contacts');
     
      var that = this;
      var emailClients = new EmailClients([], { view: contacts});

       emailClients.fetch({
        success: function(response) {
      
      if(flag_edit !='set') {
        
        if($.cookie('smartdataclients')) {
           if($.cookie('smartdataclients') != 'undefined') {
             var new1 = $.cookie('smartdataclients');
         
              $("#smartclients").val(new1); //$.cookie('smartclients',new1)  ;
        
            //console.log("cookie value" ,$.cookie('smartclients')) ;
        
             }

            }
          } 

           console.log("smart",response);
     
           that.remove_extra_views();

          
          var length2 =response.models[0].attributes.all_clients.length;
         // alert(length2);

         $(that.el).html(that.template({groups_collection: response.toJSON()}));
           $('.css-label').addClass('check');
           $('#all').addClass('check');
           if(length2 == 0){

            $("#length1").css("display","table-cell");
            $("#length1").attr('colspan','3');
            $(".check-length").html("No Record found.");

          } 
          
          return that;
        },
        error: function() {
          console.log("Data not fetched.");
        }
      });
       


	  },


    remove_extra_views: function() {
    
  
    $(".company").remove();
     //$(".company-sub").remove();
    
     $(".client").remove();
    //$(".client-sub").remove();
   
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    //$(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();

  }
 
})
})();
