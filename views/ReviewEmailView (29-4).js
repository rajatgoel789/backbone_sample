window.ReviewEmailView  = Backbone.View.extend({
 
  className: 'review-email', 

  initialize: function() {
    console.log('ReviewEmailView initialized');
  },

    events: {
    'click  #contacts': 'contacts',
    'click  #email': 'email',
    'click  #edit_contacts': 'edit_contacts',
    'click  #edit_email_contents': 'edit_email_contents',
    'click  #edit_subject_line': 'edit_subject_line',
    'click  #edit_delivery_time1':'set_delivery_time',

    'click  #send_email'  :  'send_email',
    'click  #submit_email'  :  'submit_email'
    
  },

  set_delivery_time: function() {
  if($.cookie('contacts') != null && $.cookie('contacts') == 'smart') {
   // $(".review-email").remove();
    var view = new SmartDeliveryView();
     view.render();
    //  $('.col-lg-2').after(view.el);

  }

  else {

  var view = new DeliveryView();
      view.render();
      $('.col-lg-2').after(view.el);
 
    }
  },

  contacts: function () {
    $(".review-email").remove();

    var a = $.cookie('contacts') ;

    if( a=='group') {

    var view = new ContactsGroupsView();
        view.render();
     }

    if( a=='client') {

    var view = new ContactsClientsView();
        view.render();
     }

   if(a=='smart') {
     var view = new ContactsSmartView();
        view.render();
      }

       $('.col-lg-2').after(view.el);
  },

  email: function () {
       $(".review-email").remove();
       var view = new EmailContentsView();
        view.render();
       $('.col-lg-2').after(view.el);
  },


   edit_contacts: function () {
    $(".review-email").remove();

      var a = $.cookie('contacts') ;
      console.log("a",a);
    
    if(a == 'group') {
    var view = new ContactsGroupsView();
        view.render();
     
     }

     else if (a == 'client') {
  
    var view = new ContactsClientsView();
        view.render();
    
     }
     else {
      var view = new ContactsSmartView({flag: "edit"});
        view.render();   
     }
       $('.col-lg-2').after(view.el);
  },

  edit_email_contents: function () {

     if($.cookie('contacts') != null && $.cookie('contacts') != 'smart') {
                 
    $(".review-email").remove();
 
    if($.cookie('viewlevel') != null && $.cookie('viewlevel') == 'company_level') {


    var view = new CompanyView();
        view.render();
       $('.col-lg-2').after(view.el);
      setTimeout(view.loadImages, 2000);
    }

    else{
  
      var view = new ClientView();
        view.render();
       $('.col-lg-2').after(view.el);
      setTimeout(view.loadImages, 500);

    }

   }
  },

  edit_subject_line: function () {
    $(".review-email").remove();
    var view = new EmailContentsView({flag: "edit"});
        view.render();
       $('.col-lg-2').after(view.el);
  },

 

  send_email:function(){ 
    
   $selected_post = $.cookie("post-cookie");
   posts_id =  eval($selected_post).join(",");

    var flag = $.cookie("contacts");
   // console.log("flag",flag);

    if(flag =='group') {
    
    var selected_groups1 =$.cookie("gArray");
    var selected_groups =  eval(selected_groups1).join(",");

    }

    if(flag =='client') {
    
    var selected_groups1 =$.cookie("UserArray");
    var selected_groups =  eval(selected_groups1).join(",");

    }

     if(flag =='smart') {
    
     var selected_groups1 = $.cookie("smartclients");
     var selected_groups =  eval(selected_groups1).join(",");
     alert('a');
     alert(selected_groups);


     }

   console.log("selected_groups",selected_groups);
 
   var selected_template_id = $.cookie("TempId");

   var selected_subject = $.cookie("ChangeSubject");

   var selected_body = $.cookie("ChangeBody");

   var selected_date = $.cookie("SelectDate");

   var selected_hours = $.cookie("SelectHours");

   var selected_minutes = $.cookie("SelectMinutes");

   var selected_time = $.cookie("SelectTime");


    var data = {   
            selected_post_id  : posts_id,
            groups_id         : selected_groups,
            temp_id           : selected_template_id,
            subject           : selected_subject,
            body              : selected_body,
            date              : selected_date,
            hours             : selected_hours,
            minutes           : selected_minutes,
            time              : selected_time,
            flag              : flag
        }  

       console.log ("data",data); 

      $.ajax({
          url      :  article_email,
          type     : 'POST',
          data     : JSON.stringify(data), 
          contentType : "application/json; charset=utf-8",
          dataType    : 'json',
         success: function (response) {
            console.log(response);
            if (response.time=='') {
              alert("You’ve successfully sent your email.");
            }else{
              alert("You’ve successfully scheduled your email to be delivered.");
            }

            $("#loaderdiv").show();

            $(".review-email").remove();
            $.removeCookie("post-cookie");

            if($.cookie('viewlevel') != null && $.cookie('viewlevel') == 'company_level') {

             var view = new CompanyView();
             view.render();
             
             $('.col-lg-2').after(view.el);
             setTimeout(view.loadImages, 3000);

              }

            else{
              var view = new ClientView();
              view.render();
             
             $('.col-lg-2').after(view.el);


            }
            
          },
          error: function (error) {
            console.log("failed",error);
              $("#loaderdiv").hide();
            alert("Mail not sent. Please go through this process again or Select Delivery Time for 3 days in future only!");

          
         }
      });

  },

  submit_email:function(){
  
   var smart_clients1 =$.cookie("smartclients");
   var smart_clients =  eval(smart_clients1).join(",");

   var smart_template_id = $.cookie("TempId");
   var smart_subject = $.cookie("ChangeSubject");
   var smart_body = $.cookie("ChangeBody");

   var smart_type = $.cookie("SmartType");
   var smart_frequency = $.cookie("SmartFrequency");
   
   if($.cookie("SmartDate") == null  || $.cookie("SmartDate") =='undefined' || $.cookie("SmartDate") ==''  || $.cookie("SmartHours") == null  || $.cookie("SmartHours") =='undefined' 
   || $.cookie("SmartMinutes") == null  || $.cookie("SmartMinutes") =='undefined' || $.cookie("SmartTime") == null  || $.cookie("SmartTime") =='undefined' )
     
     {
        $(".check-error").html("Please select Delivery Date and Time.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 

          return false ; 

     }


   var smart_date = $.cookie("SmartDate");
   var smart_hours = $.cookie("SmartHours");
   var smart_minutes = $.cookie("SmartMinutes");
   var smart_time = $.cookie("SmartTime");

   var data = {   
            smart_clients           : smart_clients,
            smart_template_id       : smart_template_id,
            smart_subject           : smart_subject,
            smart_body              : smart_body,
            smart_type              : smart_type,
            smart_frequency         : smart_frequency,
            smart_date              : smart_date,
            smart_hours             : smart_hours,
            smart_minutes           : smart_minutes,
            smart_time              : smart_time
        }  

    console.log ("data",data);
  
     $.ajax({
          url         :'../smart_data/',
          type        : 'POST',
          data        : JSON.stringify(data), 
          contentType : "application/json; charset=utf-8",
          dataType    : 'json',

         success: function (response) {
            console.log("sucess");
            alert("You’ve successfully scheduled your email to be delivered.");
             

             $("#loaderdiv").show();

             $(".review-email").remove();

            if($.cookie('viewlevel') != null && $.cookie('viewlevel') == 'company_level') {

             var view = new CompanyView();
             view.render();
             
             $('.col-lg-2').after(view.el);
            
              }

            else{
              var view = new ClientView();
             view.render();
             
             $('.col-lg-2').after(view.el);


            }
            

          },
          error: function (error) {
            console.log("failed");
            //alert("Something Gone wrong! Please go through the Process again.");
           
         }
      });
    

  },

  render:function () {
     $(this.el).html(this.template());
     // that.remove_extra_views();
    return this;
    
    },

  //   remove_extra_views: function() {
    
  //     $(".company").remove();
  //    //$(".company-sub").remove();
    
  //    $(".client").remove();
  //  // $(".client-sub").remove();
   
  //   $(".contacts-groups").remove();
  //   $(".contacts-clients").remove();
  //   $(".contacts-smart").remove();

  //   $(".email-contents").remove();
  //   //$(".review-email").remove();
   

  // }
 
});
