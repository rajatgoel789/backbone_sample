window.ContactsClientsView  = Backbone.View.extend({
 
	className: 'contacts-clients', 

	initialize: function() {
	  console.log('ContactsClientsView initialized'); 
	  $("#tablesorter-demo").tablesorter({sortList:[[0,0],[2,1]], widgets: ['zebra']});console.log('sorting function call');
	  //$("#options").tablesorter({sortList: [[0,0]], headers: { 3:{sorter: false}, 4:{sorter: false}}});
	  
  },

   events: {
    'click  #email': 'email',
    'click  #review': 'review',
    'click  #next': 'next',
    'click .css-checkbox':'checked_users',
    'click #all':'check_all'
	},

	email: function () {
   // alert($.cookie('UserArray'));
	
    //if( $.cookie('UserArray') == null  || $.cookie('UserArray') =='undefined' || $.cookie('UserArray').length == 2)
      if($("#UserArray").val() == null  || $("#UserArray").val() == '[]' || $("#UserArray").val() =='undefined' || $("#UserArray").val().length == 2)
     {
        $(".check-error").html("Please select at least 1 Client to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }

    else {

    $(".contacts-clients").remove();
       $(".review-email").remove();

		var view = new EmailContentsView();
        view.render();
       $('.col-lg-2').after(view.el);
     }
	},

  review: function () {
   if( $.cookie('TempId') == null  || $.cookie('TempId') == 0 || $.cookie('TempId') == '')
     {  //alert("Tempif");
        $(".check-error").html("Please select a Template to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
          return false ;
//	   if( $.cookie('UserArray') == null  || $.cookie('UserArray') =='undefined' || $.cookie('UserArray').length == 2)
   if($("#UserArray").val() == null  || $("#UserArray").val() =='undefined' || $("#UserArray").val().length == 2)
                 {
                $(".check-error").html("Please select at least 1 Client to proceed further.");
                  $(".check-error").fadeIn().css("display","block");
                  $(".check-error").delay(2000).slideUp("slow"); 
             }

     }else{
     // alert("Tempelse");
     }

    // if( $.cookie('UserArray') == null  || $.cookie('UserArray') =='undefined' || $.cookie('UserArray').length == 2)
      if($("#UserArray").val() == null || $("#UserArray").val() == '[]'  || $("#UserArray").val() =='undefined' || $("#UserArray").val().length == 2)
        {
        $(".check-error").html("Please select at least 1 Client to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
          return false ;
     }
    else {
    $(".contacts-clients").remove();
    var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);
     }
  },
  
  next: function () {
   //if( $.cookie('UserArray') == null  || $.cookie('UserArray') =='undefined' || $.cookie('UserArray').length == 2)
      if($("#UserArray").val() == null || $("#UserArray").val() == '[]'  || $("#UserArray").val() =='undefined' || $("#UserArray").val().length == 2)
    {
        $(".check-error").html("Please select at least 1 client to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }
     
    else {

    $(".contacts-clients").remove();
       $(".review-email").remove();

    var view = new EmailContentsView();
        view.render();
       $('.col-lg-2').after(view.el);
     }
  },

   checked_users: function (e) { //alert("asa");
      
    var user_checked = $(e.currentTarget).attr('data-clientid');
    var next = $(e.currentTarget).next();
    var cls = $(e.currentTarget).attr('class');
 
  
    // var a = $.cookie('UserArray') ;
     var a = $("#UserArray").val() ;

     var UserArray = [];
    if(typeof a == 'undefined' && $.trim(cls) == 'css-checkbox')
    {
      UserArray.push(parseInt(user_checked));
      $(e.currentTarget).addClass('check');
      next.addClass('check');

    }   else if(typeof a != 'undefined' && $.trim(cls) == 'css-checkbox')
    { 
       var UserArray =  $.parseJSON(a);
       for(var i = 0; i < UserArray.length; i++) 
        {
           UserArray[i] = parseInt(UserArray[i],10);
        } 
      $(e.currentTarget).addClass('check');
      next.addClass('check');
      UserArray.push(parseInt(user_checked));

    }else {
       var UserArray =  $.parseJSON(a);
       for(var i = 0; i < UserArray.length; i++) 
        {
           UserArray[i] = parseInt(UserArray[i],10);
        } 
       $(e.currentTarget).removeClass("check");

       next.removeClass("check");
        
       if($.inArray(user_checked, UserArray))
        {
          
        $.each( UserArray, function( index, value ) {
        
         // alert(value);
          //alert(index);

        if(user_checked ==  value)
        {
          removeindex=index;
        }

                  });

         // alert(removeindex);
        //  alert(UserArray) ;
       if(typeof removeindex != 'undefined' ){
    
          UserArray.splice(removeindex, 1 );}
        }
       }
     
      console.log("new UserArray", UserArray);
      $("#UserArray").val(JSON.stringify(UserArray));//alert($("#UserArray").val());
     //$.cookie('UserArray', JSON.stringify(UserArray));

   },
   
 
	check_all:function(event){ //alert("sas");
  $('#loaderdiv').show();
   var that = this;
   var emailClients = new EmailClients([], { view: contacts});

   emailClients.fetch({
   success: function(response) { console.log('all response is ',response);
   
    var clients = response.toJSON();
    var len = clients.length;//alert(len);
    //alert( $(event.currentTarget).attr('class'));
   
  //  alert( $(event.currentTarget).className));
     
    console.log("event",event.currentTarget);
     //return false ;

		//$('.css-checkbox').each(function() { //loop through each checkbox
		  
      //if ($(this).attr('class')=='css-checkbox check') { 
    if( $(event.target).hasClass("check") ){
       // alert("If call");
			$(this).removeClass('check');
			$('.css-label').removeClass('check');
		 
					var a =  $("#UserArray").val() ;// $.cookie('UserArray') ;
					var UserArray = [];
          $("#UserArray").val(JSON.stringify(UserArray)) ;
				
					/*$.each([clients],function(index,value){ //console.log(clients);
						for(var k=0;k<len;k++){ //alert(k);
						    //console.log('group id is ',value[k]['client_id']);
						   // UserArray[] = '';
						}*/
						// UserArray=[];
					//	$.cookie('UserArray', JSON.stringify(UserArray));
					//	console.log('new array is ',UserArray);
					//});
					$('#loaderdiv').hide();
				     }
       else { 
     //alert("Else Cll");
			   $(this).addClass('check');
			   $('.css-label').addClass('check');
		     var a =  $("#UserArray").val() ;
					//var a = $.cookie('UserArray') ;
					var UserArray = [];
				console.log("clients",clients) ;  
				//	$.each([clients],function(index,value){  //console.log(clients);
						for(var k=0;k<len;k++){ //alert(k);
              // alert(clients[k].client_id);
						    //console.log('group id is ',value[k]['client_id']);
						    UserArray[k] = JSON.parse(clients[k]['client_id']);
						}
            $("#UserArray").val(JSON.stringify(UserArray));
						//$.cookie('UserArray', JSON.stringify(UserArray));
						console.log('new array is ',UserArray);
				 //  	});
         
				  }
		//	 });
	$('#loaderdiv').hide();		
      }

		});
		   
		
	},
   
	render:function () {
    
      contacts = $.cookie('contacts');
      var that = this;
      var emailClients = new EmailClients([], { view: contacts});

	    emailClients.fetch({
	      success: function(response) { 
          that.remove_extra_views();
           console.log("response",response) ;
       $(that.el).html(that.template({clients_collection: response.toJSON()}));
       $("#all").addClass("check");
          if(response.length == 0){
            //alert(response.length);
            //console.log("length",$("#length1"));
            $("#length1").css("display","table-cell");
            $("#length1").attr('colspan','3');
            $(".check-length").html("No Record found.");

          } 
	       
	        return that;
	      },
	      error: function() {
	        console.log("Data not fetched.");
	      }
	    });
    
	  },
    remove_extra_views: function() {
    
  
      $(".company").remove();
     //$(".company-sub").remove();
    
     $(".client").remove();
    //$(".client-sub").remove();
   
    $(".contacts-groups").remove();
    //$(".contacts-clients").remove();
    $(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();
   

  }
 
});


 