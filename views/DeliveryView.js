window.DeliveryView  = Backbone.View.extend({
 
  className: 'delivery', 

  initialize: function() {
    console.log('DeliveryView initialized');

  },


  events: {
    
    'click  #update_delivery':'update_delivery',
  }, 

 update_delivery:function() {

  var date = $('#pick_date').val();

        var SelectDate = '';

         if(date != 'undefined')
         {
            SelectDate = date ;
            console.log("SelectDate",SelectDate);
         }
       
       $.cookie('SelectDate',SelectDate);

  
  var hours = $('#select_hours').val();
  
        var SelectHours = '';

         if(hours != 'undefined')
         {
            SelectHours = hours ;
            console.log("SelectHours",SelectHours);
         }
       
       $.cookie('SelectHours',SelectHours);


  var minutes = $('#select_minutes').val();
  
        var SelectMinutes = '';

         if(minutes != 'undefined')
         {
            SelectMinutes = minutes ;
            console.log("SelectMinutes",SelectMinutes);
         }
       
       $.cookie('SelectMinutes',SelectMinutes);


  var time = $('#smart_time').val();
  
        var SelectTime = '';

         if(time != 'undefined')
         {
            SelectTime = time ;
            console.log("SelectTime",SelectTime);
         }
       
       $.cookie('SelectTime',SelectTime);


       $("#del_tme").modal('hide');

      $(".review-email").remove();
      $( '.modal-backdrop' ).remove();
      $( 'body' ).removeClass( "modal-open" );

    var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);

  },

 render:function () {

    $(this.el).html(this.template());
    return this;
  }

  });