window.ContactsGroupsView  = Backbone.View.extend({
 
	className: 'contacts-groups', 

	initialize: function() {
	  console.log('ContactsGroupsView initialized');
	},

   events: {
    'click  #email': 'email',
    'click  #review': 'review',
    'click  #next': 'next',
    'click .css-checkbox':'checked_groups',
    'click #all':'check_all'
	},

	email: function () {
  
    if( $.cookie('gArray') == null  || $.cookie('gArray') =='undefined' || $.cookie('gArray').length == 2)
     {
        $(".check-error").html("Please select at least 1 Group to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 

     }

    else {
     
	   $(".contacts-groups").remove();
     $(".review-email").remove();
		 var view = new EmailContentsView();
        view.render();
       $('.col-lg-2').after(view.el);

     }
	},

  review: function () {

    if( $.cookie('TempId') == null  || $.cookie('TempId') == 0 || $.cookie('TempId') == '')
     {
        $(".check-error").html("Please select a Template to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
          return false ;

          if( $.cookie('gArray') == null  || $.cookie('gArray') =='undefined' || $.cookie('gArray').length == 2)
             {
                $(".check-error").html("Please select at least 1 Group to proceed further.");
                  $(".check-error").fadeIn().css("display","block");
                  $(".check-error").delay(2000).slideUp("slow"); 
             }
     }

     else if( $.cookie('gArray') == null  || $.cookie('gArray') =='undefined' || $.cookie('gArray').length == 2)
     {
        $(".check-error").html("Please select at least 1 Group to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
          return false ;
     }

    else {
  
    $(".contacts-groups").remove();
    var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);

     }
    
  },
  
  next: function () {

     if( $.cookie('gArray') == null  || $.cookie('gArray') =='undefined' || $.cookie('gArray').length == 2)
     {
        $(".check-error").html("Please select at least 1 group to proceed further.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }

    else {

    $(".contacts-groups").remove();
     $(".review-email").remove();
    var view = new EmailContentsView();
        view.render();
       $('.col-lg-2').after(view.el);
     }
  },

   checked_groups: function (e) {
   //	$.removeCookie("gArray");
   
   var group_checked = $(e.currentTarget).attr('data-groupid');

   var client_count = $(e.currentTarget).attr('data-clients');
   //alert(client_count);

   if(client_count == 0)
   {
 
   $(".check-error").html("Group cannot be checked.It has no clients in it.");
    $(".check-error").fadeIn().css("display","block");
    $(".check-error").delay(2000).slideUp("slow"); 
    
    return false ;
   }


   var next = $(e.currentTarget).next();

    var cls = $(e.currentTarget).attr('class');
 
  
    var a = $.cookie('gArray') ;
     var gArray = [];
    if(typeof a == 'undefined' && $.trim(cls) == 'css-checkbox')
    {
      gArray.push(parseInt(group_checked));
      $(e.currentTarget).addClass('check');
      next.addClass('check');

    }   else if(typeof a != 'undefined' && $.trim(cls) == 'css-checkbox')
    { 
       var gArray =  $.parseJSON(a);
       for(var i = 0; i < gArray.length; i++) 
        {
           gArray[i] = parseInt(gArray[i],10);
        } 
        
      $(e.currentTarget).addClass('check');
      next.addClass('check');
      gArray.push(parseInt(group_checked));

    }else {
       var gArray =  $.parseJSON(a);
       for(var i = 0; i < gArray.length; i++) 
        {
           gArray[i] = parseInt(gArray[i],10);
        } 
       $(e.currentTarget).removeClass("check");

       next.removeClass("check");
        
       if($.inArray(group_checked, gArray))
        {
          
        $.each( gArray, function( index, value ) {
        
         // alert(value);
          //alert(index);

        if(group_checked ==  value)
        {
          removeindex=index;
        }

                  });

          //alert(removeindex);

          gArray.splice(removeindex, 1 );
        }
       }
     
      console.log("new gArray", gArray);
     $.cookie('gArray', JSON.stringify(gArray));

   },

   	check_all:function(event){
	$('#loaderdiv').show();
      var that = this;
      var emailGroups = new EmailGroups();
    
      emailGroups.fetch({
      success: function(response) { //console.log('all response is ',response);
	
      var groups = response.toJSON();
      var len = groups.length;//alert(len);

		 $('.css-checkbox').each(function() { //loop through each checkbox

		  if ($(this).attr('class')=='css-checkbox check') {
			$(this).removeClass('check');
			$('.css-label').removeClass('check');
			
          var a = $.cookie('gArray') ;
          var gArray = [];
					$.each([groups],function(index,value){
						for(var k=0;k<len;k++){ //alert(k);
						    //console.log('group id is ',value[k]['group_id']);
						   // gArray[k] = '';
               
						}

						gArray = [];
						$.cookie('gArray', JSON.stringify(gArray));
						console.log('new array is ',gArray);
				  	});
				$('#loaderdiv').hide();
				   }
				
       else
       {
		     
        $(this).addClass('check');
			  $('.css-label').addClass('check');
			
				
          var a = $.cookie('gArray') ;
          var gArray = [];
					$.each([groups],function(index,value){
						for(var k=0;k<len;k++){ //alert(k);
						    //console.log('group id is ',value[k]['group_id']);
                //alert(response.models[k].attributes.client_count);
                if(response.models[k].attributes.client_count > 0)
                 {

						        gArray[k] = JSON.parse(value[k]['group_id']);
                    // $(this).removeClass('check');
                    // $('.css-label').removeClass('check');
                }
						}

						$.cookie('gArray', JSON.stringify(gArray));
						console.log('new array is ',gArray);
					});
					
					$('#loaderdiv').hide();
				 }

			 });
			
			}
		   
		});
	},
   
	render:function () {
      var that = this;
      var emailGroups = new EmailGroups();

	    emailGroups.fetch({
	      success: function(response) {
          that.remove_extra_views();
	       $(that.el).html(that.template({groups_collection: response.toJSON()}));

          if(response.length == 0){
            //alert(response.length);
            //console.log("length",$("#length1"));
            $("#length1").css("display","table-cell");
            $("#length1").attr('colspan','3');
            $(".check-length").html("No Record found.");

          } 
          
	        return that;
	      },
	      error: function() {
	        console.log("Data not fetched.");
	      }
	    });
    
	  },

    remove_extra_views: function() {
   
   
      $(".company").remove();
     //$(".company-sub").remove();
    
     $(".client").remove();
   // $(".client-sub").remove();
   
   // $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();


  }
 
});
