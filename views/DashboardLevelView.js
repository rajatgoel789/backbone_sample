window.DashboardLevelView = Backbone.View.extend({
  
  className: "dashboard-level",
  
  initialize: function () {
  	console.log('DashboardLevel initialized');
   // if( $.cookie('viewlevel') == null  || $.cookie('viewlevel') =='undefined' )
    // { 
      $.cookie('viewlevel','company_level');  
         // } 
  },

  events: {
    'click  ul#level li'          : 'levelSelected',
    'click  .email-group'         : 'emailGroupModal',
    'click  .email-client'        : 'emailClientModal',
    'click  .email-smartcontent'  : 'emailSmartContentModal',
   // 'click  .plus-url'            : 'userPlusUrlModal'   ,      
  },

  render: function () {
    $(this.el).html(this.template());
    return this;
  },

  levelSelected: function (e) {
    var levelchoice = $(e.currentTarget).attr('data-level');
    if(levelchoice == 'client') {

       var level = 'client_level';
        $.cookie('viewlevel',level);


       $("#loaderdiv").show(); 

       //$('.client').remove(); 
      $(".client-sub").remove();

      $('.company').remove(); 
      $(".company-sub").remove();
      $("#client-article").remove();
      $("#article-detail").remove();

      $(".contacts-groups").remove();
      $(".contacts-clients").remove();
      $(".contacts-smart").remove();

      $(".email-contents").remove();
      $(".review-email").remove();
      
      // Subviews 
      
      var clientsubview = new ClientSubView();
      clientsubview.render();
      $('br').after(clientsubview.el);   

      this.clientview = new ClientView();
      this.clientview.render();
      $('.col-lg-2').after(this.clientview.el);
     // $("#loaderdiv").hide(); 

     }else { 

       var level = 'company_level';
        $.cookie('viewlevel',level);


      $("#loaderdiv").show(); 
     
     // $('.company').remove(); 
      $(".company-sub").remove();
    
      $('.client').remove(); 
      $(".client-sub").remove();
      $("#article-detail").remove();
      $("#client-article").remove();
      $(".contacts-groups").remove();
      $(".contacts-clients").remove();
      $(".contacts-smart").remove();

      $(".email-contents").remove();
      $(".review-email").remove();

      // Subviews 
      var companysubview = new CompanySubView();
         companysubview.render();
        $('br').after(companysubview.el);  


      this.companyview = new CompanyView();
      this.companyview.render();  
      $('.col-lg-2').after(this.companyview.el);

       setTimeout(this.companyview.loadImages, 3000);

    } 
  },

  emailGroupModal: function (e) {
 
 if( $.cookie('post-cookie') == null  || $.cookie('post-cookie') =='undefined' || $.cookie('post-cookie').length == 2)
     {
        $(".check-error").html("Please select an Article first from Dashboard.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }
   else {

    $(".company").remove();
    $(".client").remove();
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();
   
  
     var contacts = 'group';
     $.cookie('contacts',contacts);

     contactsview = new ContactsGroupsView();
     contactsview.render();
     
     //console.log("Contact Group View",contactsview.el);
     $('.col-lg-2').after(contactsview.el);
    }
    $("#UserArray").val('[]');
    $("#smartclients").val('[]');
     //$.removeCookie("UserArray");
    // $.removeCookie("smartclients");
  },

  emailClientModal: function (e) {
   
   if( $.cookie('post-cookie') == null  || $.cookie('post-cookie') =='undefined' || $.cookie('post-cookie').length == 2)
     {
        $(".check-error").html("Please select an Article first from Dashboard.");
          $(".check-error").fadeIn().css("display","block");
          $(".check-error").delay(2000).slideUp("slow"); 
     }
   else {

    $(".company").remove();
    $(".client").remove();
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();

     var contacts = 'client';
     $.cookie('contacts',contacts);

     clientsview = new ContactsClientsView();

     test = new  EmailClients([], { view: contacts});
     
     clientsview.render();
     $('.col-lg-2').after(clientsview.el);
     
     }

      $.removeCookie("gArray");
     // $.removeCookie("smartclients");
      $("#smartclients").val('[]');
  },

  emailSmartContentModal: function (e) {
     $("#UserArray").val('[]');
     // $.removeCookie("UserArray");
      $.removeCookie("gArray");
      $.removeCookie("SelectDate");
      $.removeCookie("SelectHours");
      $.removeCookie("SelectMinutes");
      $.removeCookie("SelectTime");


      $(".company").remove();
      $(".client").remove();
      $(".contacts-groups").remove();
      $(".contacts-clients").remove();
      $(".contacts-smart").remove();

      $(".email-contents").remove();
      $(".review-email").remove();

      $(".delivery").remove();
      
     var that = this;

     that.removetemplates();

     var contacts = 'smart';
     $.cookie('contacts',contacts);

      var emailClients = new EmailClients([], { view: contacts});

      var AllClientsArray = [];
   
      emailClients.fetch({
        success: function(response) {
      
          res = response.toJSON();
          console.log("res",res);
          for(var ix = 0; ix< res[0].all_clients.length ; ix++) {
             AllClientsArray.push(parseInt(res[0].all_clients[ix].client_id));
          }
          console.log("AllClientsArray", AllClientsArray);
     
           //$.cookie('smartclients', JSON.stringify(AllClientsArray));
            $("#smartclients").val(JSON.stringify(AllClientsArray)) ;
           var dd = [];
           var c = res[0].smart_data[0].recipients;
           if(c != '') {
           var b = c.split(",");
           for(var i=0; i< b.length; i++){
               
              dd.push(parseInt(b[i]));
            }
            console.log("dd", dd);
            $.cookie('smartdataclients',JSON.stringify(dd));
            }else {
              $.cookie('smartdataclients','');
            }
             smartview = new ContactsSmartView();
             smartview.render();
             $('.col-lg-2').after(smartview.el);
          
         },

         error: function() {
          console.log("Data not fetched.");
        }
      });
  },

  removetemplates: function() {
    $(".company").remove();
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".email-contents").remove();
    $(".contacts-smart").remove();
    $(".review-email").remove();

  } ,
   // userPlusUrlModal: function (e) { // alert("call");
   //   this.removetemplates();
   //   this.userPlusUrlModalView= new UserPlusUrlModalView();
   //   this.userPlusUrlModalView.render();
   //   $("body").append(this.userPlusUrlModalView.el);
  
   // }
});
