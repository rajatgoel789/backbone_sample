window.CompanyView = Backbone.View.extend({
 
	className: 'company', 

	initialize: function(options) {
	  console.log('CompanyView initialized');
      this.options = options || {};
      selected_category = this.options.dinesh;
	    if(this.model){
	      console.log("MODEL DATA COMPLETE", this.model);
	      var modelid=this.model.get('id');
	       if(this.model.get('article_like')  == 'like-active') {
	      	this.model.get('units').push(modelid);
            $("#tiles").find("[data-id='" + modelid + "']").find(".selected-post").addClass(this.model.get('article_like'));
            } else {
               this.model.get('units').splice( $.inArray(modelid, this.model.get('units')), 1 );
               $("#tiles").find("[data-id='" + modelid + "']").find(".selected-post").removeClass('like-active');
            }
            if(this.model.get('tile_like')  == 1) {
              var tileId = this.model.get('tile_id');
              $("#tiles").find("[data-id='" + tileId + "']").find(".tile_like").addClass('like-active');
              $("#tiles").find("[data-id='" + tileId + "']").find(".tile_dislike").removeClass('dislike-active');
            }
            else {
              var tileId = this.model.get('tile_id');
              $("#tiles").find("[data-id='" + tileId + "']").find(".tile_dislike").addClass('dislike-active');
              $("#tiles").find("[data-id='" + tileId + "']").find(".tile_like").removeClass('like-active');
            }
	    }
	},
   events: {
     'click  .tile_like'     : 'userTileDisLike',         
     'click  .tile_dislike'  : 'userTileDisLike',
     'click  .tile_detail'   : 'userTileDetail',
     'click  .selected-post' : 'showSelectedPost' ,
     'click  a.article-remove' : 'deleteArticle',
  
     },
	
	render:function () {
	    var that = this;
	    var tiles = new Tiles();
	    tiles.fetch({
	      success: function(response) {
	      	that.remove_extra_views();
	        $(that.el).html(that.template({tilesCollection: response.toJSON()}));
            // setTimeout(that.loadImages, 2000);
	        return that;
	      },
	      error: function() {
	        console.log("Data not fetched.");
	      }
	    });
	},

	remove_extra_views: function() {
		
     //$(".company").remove();
     //$(".company-sub").remove();
    
    $(".client").remove();
    $(".client-sub").remove();
   
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".contacts-smart").remove();

    $(".email-contents").remove();
    $(".review-email").remove();
   

  },
	userTileDetail: function (e) {
      $("#client-article").remove();

      var tileid= $(e.currentTarget).attr('data-tileid');


	  var tiles = new Tiles();
   $.ajax({
          url      :  '../article_detail/'+tileid,
         // contentType : "application/json; charset=utf-8",
          dataType    : 'json',
         success: function (response) {

	          if($("#client-article").length <=1)
	          {
		            clientArticleDetail = new ClientArticleDetail();
			        $(clientArticleDetail.el).html(clientArticleDetail.template({tile_model: response}));
				    console.log("Details",clientArticleDetail);
				    $("body").append(clientArticleDetail.el);
				    $("#productclk_model").modal("show");	
	           }
          },
          error: function (error) {
          alert("Error");
         }
      });

 	 /* tiles.fetch({
        success: function(response) { //alert(tileid); 
			console.log("Complete Response",response);
			var tileid= $(e.currentTarget).attr('data-tileid'); //alert(tileid);
	        var tileModel= response.where({'id':parseInt(tileid)  });
		    clientArticleDetail = new ClientArticleDetail({model: tileModel, collection: tiles});
	        $(clientArticleDetail.el).html(clientArticleDetail.template({tile_model: tileModel}));
		    console.log("Details",clientArticleDetail);
		   $("body").append(clientArticleDetail.el);
		   $("#productclk_model").modal("show");	
        },
        error: function() {
           console.log("Data not fetched.");
      }
    });*/
  },
	
	userTileLike: function (e) {
		 var tileid =$(e.currentTarget).attr('data-tileid'); alert(tileid);
		 var tileLike = new Tile({
		   tile_id: tileid,
		   tile_like: 1,
		   tile_dislike: 0,	
         });	
		
		tileLike.save({},{
			success:function(model,response,xhr){

				if(response.error_msg) {
				//$(e.currentTarget).disable();
				  $(e.currentTarget).css('pointer-events', 'none');
				
				  if ($(e.currentTarget).hasClass('like-active')) {
					}else{
			          $(e.currentTarget).addClass("like-active");
	 				}
					alert(response.error_msg);
	                // what if this property exists.
	            }
				else
				{
				   tileLike.set({tile_like :1})
				   alert(tileLike.get('tile_like'));
				   var tile_like_status =tileLike.get('tile_like') ;
				    if (tile_like_status==1) {
					
				     $(e.currentTarget).css('pointer-events', 'none');
				     $(e.currentTarget).parent().prev().find("a").css('pointer-events', 'none')
				      console.log($(e.currentTarget).parent().prev().find("a"));
				  //   $(e.currentTarget).parent().siblings("dt .tile_dislike").css('pointer-events', 'none');  
				    
				    if($(e.currentTarget).hasClass('like-active')) {
					}else{
			           $(e.currentTarget).addClass("like-active");
	                }
					//code		
				    }
				  
				}
			
			},
			error: function(model,response,xhr){
			  //alert("error");
			}
		});
  },
	
	userTileDisLike: function (e) { 
		 var tileid =$(e.currentTarget).attr('data-tileid'); //alert(tileid);
		 var currentclass=$(e.currentTarget).attr('class');   //alert(currentclass) ;
		  if (currentclass.toLowerCase().indexOf("tile_like") >= 0) {
		    var tl = 1 ;
		    var tdl=0 ;
		}else if(currentclass.toLowerCase().indexOf("tile_dislike") >= 0){
	
		var tl = 0 ;
		var tdl=1 ;
		}
		else {
			var tl = 0 ;
			var tdl= 0 ;
		}
	   var tileDisLike = new Tile({
		  tile_id: tileid,
		  tile_like: tl,
		  tile_dislike: tdl,
	   });
	
		  	
		tileDisLike.save({},{
			success:function(model,response,xhr){
			//
			
			if(response.error_msg)
			{
			//$(e.currentTarget).disable();
			 alert(response.error_msg);
                         // what if this property exists.
                        }
			else
			{
			  if (response.like == 1) {
				//code
			  
			  
			       tileDisLike.set({tile_like :1,tile_dislike :0})
			       //alert(tileDisLike.get('tile_like'));
			       var tile_like_status =tileDisLike.get('tile_like') ;
			       if (tile_like_status==1) {
				
					$(e.currentTarget).css('pointer-events', 'none');
					$(e.currentTarget).parent().prev().find("a").css('pointer-events', 'auto')
					console.log($(e.currentTarget).parent().prev().find("a"));
				   //   $(e.currentTarget).parent().siblings("dt .tile_dislike").css('pointer-events', 'none');  
	         			     if ($(e.currentTarget).parent().prev().find("a").hasClass('dislike-active'))
					 {
						$(e.currentTarget).parent().prev().find("a").removeClass("dislike-active");
					 }
			    
					 if($(e.currentTarget).hasClass('like-active')){
						}
						else
						{
						$(e.currentTarget).addClass("like-active");
						}

			    }
			    
			    }else if(response.dislike == 1)
			    { //alert("dislike call");
				
				 tileDisLike.set({tile_like :0,tile_dislike :1}) ;
			      //  alert(tileLike.get('tile_like'));
			       var tile_like_status =tileDisLike.get('tile_like') ;
			       if (tile_like_status==0) {
				
					$(e.currentTarget).css('pointer-events', 'none');
					$(e.currentTarget).parent().next().find("a").css('pointer-events', 'auto');
					 if ($(e.currentTarget).parent().next().find("a").hasClass('like-active')) {
						$(e.currentTarget).parent().next().find("a").removeClass("like-active");
						
					 }
					
					 if($(e.currentTarget).hasClass('dislike-active')){
						}
						else
						{
						$(e.currentTarget).addClass("dislike-active");
						}
				
			    }
				
			}
			
			
			}
			},
			error: function(model,response,xhr){
			alert("error");
			}
		});
	
   },
 deleteArticle: function(e) { 
	
	 
   var r=confirm("Are you sure to delete this content ");
    if (r==true){ }else{ return false ; }
 
    
    var articleId = $(e.currentTarget).attr('data-tileid');

    //alert(articleId);
    var tile = new Tile({ id: articleId, isdelete: 1 });
    var tiles = new Tiles();
    var that = this;  //return false ;
    tile.destroy({
        data: JSON.stringify(tile),
        contentType: 'application/json',
        dataType: "text",
      success: function(response) {
      	$(".company").remove();
      	$("#loaderdiv").show();
        var companyview = new CompanyView();
       tiles.fetch({
         success: function(response) {
           $(companyview.el).html(companyview.template({tilesCollection: response.toJSON()}));
            $(".col-lg-2").after(companyview.el);
            setTimeout(companyview.loadImages, 2000);

        }, error: function(error) {
        console.log('error', error);
      }
      });
     //   that.closeModal();
      },
      error: function(error) {
      		$("#loaderdiv").hide();
        console.log('error', error);
      }
    });

  },
   showSelectedPost: function (e) { 
   	 var class_check = $(e.currentTarget).attr('class');
	    var tileModel = new Tile(); 
	    var postId = $(e.currentTarget).attr('data-postid');
	    tileModel.set({selected_post : "checked"});
	    if(tileModel.get("selected_post") == "checked"){
	       var selected_post = tileModel.get('units');
	       selected_post = [];
	       
           if($.trim(class_check) == "selected-post" && $.cookie('post-cookie') == undefined){

	          $(e.currentTarget).addClass("like-active");
	          selected_post.push(postId);

	       }else if($.trim(class_check) == "selected-post" && $.cookie('post-cookie') != undefined){

		       var selected_post = $.parseJSON($.cookie('post-cookie'));
		        for(var i = 0; i < selected_post.length; i++)  {
		          selected_post[i] = parseInt(selected_post[i], 10);
		        }
		        $(e.currentTarget).addClass("like-active");
		        selected_post.push(parseInt(postId));
		     }else {
		        var selected_post = $.parseJSON($.cookie('post-cookie'));
               for(var i = 0; i < selected_post.length; i++)  {
                  selected_post[i] = parseInt(selected_post[i], 10);
                }	 

                  if($.inArray(postId, selected_post))
			        {
			          
			        $.each( selected_post, function( index, value ) {
			        
			         // alert(value);
			          //alert(index);

			        if(postId ==  value)
			        {
			          removeindex=index;
				   selected_post.splice(removeindex, 1 );
			        }

			                  });

			          //alert(removeindex);

			        // selected_post.splice(removeindex, 1 );
			        }


              //selected_post.splice( $.inArray(postId, selected_post), 1 );
	       	  $(e.currentTarget).removeClass("like-active");
	       }
            
			console.log("selected_post",JSON.stringify(selected_post));
	        $.cookie('post-cookie', JSON.stringify(selected_post));
	     }
   },

   loadImages: function(){  $("#loaderdiv").hide();
      // Prepare layout options.
      console.log(" Laod IMages Call ");
      var handler = null,
          page = 2,
          isLoading = false,
          apiURL = company_filter,
          error = {} ;

      var options = { 
        itemWidth: 257, 
        autoResize: true, 
        container: $('#tiles'),
        offset: 20 ,
	    fillEmptySpace: false,
        flexibleWidth: 257 
      };
      
      applyLayout();
      /**
       * When scrolled all the way to the bottom, add more tiles.
       */
		function onScroll(event) {
		//	console.log("error.error", error.error);
		console.log("isLoading", isLoading);
			if(!isLoading) {

	          var closeToBottom = ($(window).scrollTop() + $(window).height() > $(document).height() - 100);
	          if(closeToBottom) {  //console.log("Currentview",$(".company"));
	          // alert(typeof $(".company").length); alert(typeof 1) ;
	          	if(error.error == undefined &&  $(".company").length==1){
	              loadData();
	            }
	          }
	        }
	    };   


    /**
       * Refreshes the layout.
    */
    function applyLayout() {
        options.container.imagesLoaded(function() {
          // Create a new layout handler when images have loaded.
          handler = $('#tiles li');
          handler.wookmark(options);
        });
      };

      /**
       * Loads data from the API.
       */
      function loadData() {
        isLoading = true;
        $('#loaderCircle').show();
        if(selected_category){
          data =  { category_id : selected_category, page: page,pagesize: 25};
        }else {
          data = {page: page,pagesize: 25};
        }
        $.ajax({
          url: apiURL,
          dataType: 'json',
          data: data, // Page parameter to make sure we load new data
          success: onLoadData
        });
      };

      /**
       * Receives data from the API, creates HTML for images and updates the layout
       */
      function onLoadData(data) {
	    if(data.error) { 
          error['error'] = data.error;
          console.log("error.error", error.error);
          isLoading = true;
          $('#loaderCircle').hide();
        }else{

	    if($.cookie('post-cookie')) {
	      var cookieValues = $.parseJSON($.cookie('post-cookie'));
	      for(var i = 0; i < cookieValues.length; i++)  {
	        cookieValues[i] = parseInt(cookieValues[i], 10);
	      }
	    }
        isLoading = false;
        $('#loaderCircle').hide();

        // Increment page index for future calls.
        page++;

        // Create HTML for the images.
        var html = '';
        var i=0, length=data.length, image;
        for(; i<length; i++) {
            image = data[i];
          if(image.image=='images/DefaultIcon.png' || image.image=='undefined'  || image.image==undefined  || image.image==''  ){ 
            var img=default_img ;
          }else{
            var img=image.image ;
          }
         // alert(img) ;

            if(image.tile_like ==0 && image.tile_dislike==1 ){
               active = 'dislike-active';
            }else {
              	active = '';
            } 

            if(image.tile_like ==1 && image.tile_dislike==0){
              like = 'like-active';
            }else {
              like = '';
            }

            var index = $.inArray(image.id, cookieValues);
            if(index >= 0) {
              select = 'like-active';
            }else {
              select = '';
            }
            console.log("Image",image.source);
 
            if(image.source == ""){ image.source="Source"; }
            html += '<li data-id="' + image.id +'">';
            html += '<span class="rt_date">' + image.post_date +'</span><div class="prod_mid">';
            if(img !=default_img)
             {
             	html+='<a class="tile_detail"     data-tileid="' + image.id +'" href="javascript:void(0);">'
         	 html +=  '<img src="'+ img  +'" id="gggm" alt="product1" width="200" height="'+Math.round(image.height/image.width*200)+'" /> ';
             }
            else{ 
            html +='<span class=""> ';
 if(image.first_words.length >=110){  
             html+=image.first_words.substring(0,110)+"..." ;
                }else{ 
              html+=  image.first_words  ;  
                }


            //+ image.title +' </span>  ';
             html+='</span>';
             }
             html +='<div class="prodetailstes">';
          
             if(img !=default_img)
             {
            html += '<a href="'+image.url+'" target="_blank" class="web_nm">'+ image.source +'</a>';
            html +=  '<p>'+ image.title +'</p> ';
            }else
            {

            html += '<a href="'+image.url+'" target="_blank" class="web_nm link-dash">'+ image.source +'</a>';
            html+='  <p class=" article_title_popup info-link-hd">';
            html+=image.title ;
           /*  if(image.first_words.length >=110){  
             html+=image.first_words.substring(0,110)+"..." ;
                }else{ 
              html+=  image.first_words  ;  
                }*/
               html+="</p>";  

            }	
            html +='</div></div><div class="prod_ft"><dl>';
           // html += '<dt><a href="javascript:void(0);"  data-tileid="'+ image.id +'"  class="tile_dislike  '+ active +'" ><span class="glyphicon glyphicon-thumbs-down"></span></a></dt>';
            html += '<dt><a href="javascript:void(0);"  data-tileid="'+ image.id +'"  class="tile_like '+ like +'"><span class="glyphicon glyphicon-thumbs-up"></span></a></dt>';
     		html+='<dt><a href="javascript:void(0);"    data-tileid="'+image.id+'"  class="article-remove"><span class="glyphicon glyphicon-remove"></span></a></dt> ';
			html += '<dt><a href="javascript:void(0);"   data-tileid="'+ image.id +'"  class="tile_detail"   href="#productclk_model" data-toggle="modal" data-target="#productclk_model"><span class="glyphicon glyphicon-new-window"></span></a></dt>';
            html += '<dt><a href="javascript:void(0);"  class="selected-post  ' +select + '" data-postId= "'+ image.id  +'"><span class="glyphicon glyphicon-check"></span></a></dt>';
            html += '</dl></div> </li>';
        }

        // Add image HTML to the page.
        $('#tiles').append(html);

        // Apply layout.
        applyLayout();
       }
      };
 
       
      $(document).bind('scroll', onScroll);
      

      // Load first data from the API.

  }		
    
});
