window.HeaderView = Backbone.View.extend({
 
	className: 'header', 

	initialize: function() {
	  console.log('HeaderView initialized');
	},

	events: {
    'click  .navbar-brand': 'logo'
   
	},
	logo: function () {
      $(".contacts-group").remove();
	},
	render:function () {
	  $(this.el).html(this.template());
	  return this;
	}
 
});
