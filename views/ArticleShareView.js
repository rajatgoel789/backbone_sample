window.ArticleShareView = Backbone.View.extend({
 
	className: 'article-share', 
    events: {
       'click .linkedin-btn img' : 'shareArticleWithLinkedin',
       'click .facebook-btn img' : 'shareArticleWithFacebook',
       'click .twitter-btn img'  : 'shareArticleWithTwitter'
    },

	initialize: function() {
	  window.fbAsyncInit = function() {
        FB.init({appId: '1446922158880108', status: true, cookie: true,
        xfbml: true});
      };
      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
        '//connect.facebook.net/en_US/all.js';
       document.getElementById('fb-root').appendChild(e);
      }());
	  console.log('ArticleShareView initialized');
	},
	
	render:function () { 
	  $(this.el).html(this.template());
	  return this;
	},

	shareArticleWithLinkedin:  function () {
      console.log("shareArticleWithLinkedin");
      attributesValue = this.collection;//_.pluck(this.model, 'attributes');
   	  url = attributesValue[0].url;
      window.open('http://www.linkedin.com/shareArticle?mini=true&url='+ url +'&source=LinkedIn','NewWin',
            'toolbar=no,status=no,width=350,height=535');
    },

    shareArticleWithFacebook: function (e) { console.log("clctn",this.collection);
   	  e.preventDefault();
   	  attributesValue = this.collection;//_.pluck(this.model, 'attributes');
   	  url = attributesValue[0].url;
   	  image = attributesValue[0].image;
   	  FB.ui(
      {
        method: 'feed',
        //name: 'This is the content of the "name" field.',
        link: url,
        picture: image,
        //caption: 'This is the content of the "caption" field.',
        //description: 'This is the content of the "description" field, below the caption.',
        message: ''
      });
   	  this.remove();
   	  $('.modal-backdrop').modal('hide')
      $(".modal-backdrop.in").css("opacity", 0);
      $(".modal-backdrop.in").remove();
    },

   shareArticleWithTwitter : function (e)  {
    e.preventDefault();
    attributesValue = this.collection;//_.pluck(this.model, 'attributes');
    url = attributesValue[0].url;
    var title = 'test';
    window.open('http://twitter.com/share?url=' + url + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
}
});
