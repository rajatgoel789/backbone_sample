(function() {
    var flag_edit = "";
    var flag_change = 0;
    var renderdata = 0;
    var flag_sub = 0;
    var flag_body = 0;
    var res ;

window.EmailContentsView  = Backbone.View.extend({
 
  className: 'email-contents', 

  initialize: function(options) {
    console.log('EmailContentsView initialized',options);
    if(typeof options != 'undefined') {
      
    flag_edit ="set";
    //alert(flag_edit);
    this.options = options;
    this.flag =  this.options.flag;
    console.log("this.flag",this.flag);
    }

  },
  
    events: {
    'click  #contacts': 'contacts',
    'click  #review': 'review',
    'click  #next': 'next',
    'change  #select_template': 'change_template',
    'change #template_subject': 'change_subject',
    'change #template_body':'change_body'
  },

  contacts: function () {
    $(".email-contents").remove();
     $(".review-email").remove();

    var a = $.cookie('contacts') ;

    if( a =='group') {

    var view = new ContactsGroupsView();
        view.render();
     }

  if(a =='client') {
     var view = new ContactsClientsView();
        view.render();
      }

  if(a =='smart') {
     var view = new ContactsSmartView();
        view.render();
      }

    $('.col-lg-2').after(view.el);

  },

  review: function () {
 
   if( $.cookie('TempId') == null  || $.cookie('TempId') == 0 || $.cookie('TempId') == '')
     {
        $(".check-error").html("Please select a Template to proceed further.");
          //$(".check-error").fadeIn().css("display","block");
          //$(".check-error").delay(2000).slideUp("slow"); 
     }

    else {

      $(".email-contents").remove();
      var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);
     }
  },

    next: function () {

    if( $.cookie('TempId') == null  || $.cookie('TempId') == 0 || $.cookie('TempId') == '')
     {
        $(".check-error").html("Please select a Template to proceed further.");
         // $(".check-error").fadeIn().css("display","block");
          //$(".check-error").delay(2000).slideUp("slow"); 
     }

    else {

      $(".email-contents").remove();
       $(".review-email").remove();
      var view = new ReviewEmailView();
        view.render();
       $('.col-lg-2').after(view.el);
     }
  },

  change_template: function (e) {
        flag_sub =1;
        flag_body =1;

        var id = $("#select_template").children(":selected").attr("id");
        this.render(id);

         var temp_selected = $(e.currentTarget).val();

         var a = $.cookie('TempId') ;

        var TempId = '';

         if(temp_selected != '')
         {
            TempId = parseInt(temp_selected);
         }
       
          $.cookie('TempId', TempId);

       
         console.log("Render Data",renderdata);

         $.each( renderdata, function( index, value ) {
          console.log("object",value);
        
          if(id == value.temp_id){
         // alert("");
          var subject = value.data.mail_subject;
          //alert(subject);
          console.log("harry",subject);
          var body = value.data.mail_body;
          //alert(body);
        
           $.cookie('ChangeSubject',subject);
           //alert($.cookie('ChangeSubject',subject));
           $.cookie('ChangeBody',body);
          
            }
          });

       
       $.removeCookie("smartdatabase_tempid");
       $.removeCookie("smartdatabase_subject");
        $.removeCookie("smartdatabase_body");

  },

   change_subject: function (e) {

   var subject2 = $('#template_subject').val(); 
   var subject1 = encodeURI(subject2);
  
        var ChangeSubject = '';

         if(subject1 != 'undefined')
         {
            ChangeSubject = subject1 ;
            console.log("ChangeSubject",ChangeSubject);
         }
       
       $.cookie('ChangeSubject',ChangeSubject);

   },
  
  change_body: function (e) {

   var body2 = $('#template_body').val();

      var body1 = encodeURI(body2);
 
        var ChangeBody = '';

         if(body1 != 'undefined')
         {
           ChangeBody = body1 ;
           console.log("ChangeBody",ChangeBody);
         }
       
       $.cookie('ChangeBody',ChangeBody);

   },

    remove_extra_views: function() {
    
      $(".company").remove();
     //$(".company-sub").remove();
    
     $(".client").remove();
    //$(".client-sub").remove();
   
    $(".contacts-groups").remove();
    $(".contacts-clients").remove();
    $(".contacts-smart").remove();

    //$(".email-contents").remove();
    $(".review-email").remove();

  },

  render:function (id,res) {
     var that = this; 
    $.ajax({
        url: "../load_templates_data",
        dataType: "json",
        success: function(result) {
          renderdata=result;
                  console.log("ID CAll",res);
                  console.log("success CAll",result);
           

      if( typeof res === 'undefined') {

       //alert("no");
     
       } else {

        //alert("yes");

            var temp = res[0].smart_data[0].temp_id;

            console.log("temp",temp);
            if(temp !='' && flag_change==0) {

            $.cookie('smartdatabase_tempid',temp);
            flag_change=1;
             }

          if(flag_edit !='set' ) {
            if($.cookie('smartdatabase_tempid')) {
            if($.cookie('smartdatabase_tempid') != 'undefined') {
             var new_temp = $.cookie('smartdatabase_tempid');
         
             $.cookie('TempId',new_temp);
        
             console.log("cookie value" ,$.cookie('TempId')) ;
         
           }

         }  
        }
          

           var subject1 = res[0].smart_data[0].subject;
           var subject = decodeURI(subject1);
            console.log("subject",subject);
           
            if(subject !='' ) {
            $.cookie('smartdatabase_subject',subject);
            
            }else {
              $.cookie('smartdatabase_subject','');
             }

           if(flag_edit !='set' && flag_sub == 0) {
            if($.cookie('smartdatabase_subject')) {
            if($.cookie('smartdatabase_subject') != 'undefined') {
             var new_subject = $.cookie('smartdatabase_subject');
         
             $.cookie('ChangeSubject',new_subject);
        
             console.log("cookie value" ,$.cookie('ChangeSubject')) ;
         
           }

         }  
        }
       
         var body1 = res[0].smart_data[0].body;
           var body = decodeURI(body1);
            console.log("body",body);
           
            if(body !=''  ) {
            $.cookie('smartdatabase_body',body);
            }else {
              $.cookie('smartdatabase_body','');
             }

           if(flag_edit !='set' && flag_body == 0 ) {
            if($.cookie('smartdatabase_body')) {
            if($.cookie('smartdatabase_body') != 'undefined') {
             var new_body = $.cookie('smartdatabase_body');
         
             $.cookie('ChangeBody',new_body);
        
             console.log("cookie value" ,$.cookie('ChangeBody')) ;
         
           }

         }  
        }

    }
        that.remove_extra_views();
    
        $(that.el).html(that.template({templates_data: result, temp_id : id}));
     
         
        },
        error :function(result){ 
         console.log("error",result);
          }
      });
    },
     
  });
})();