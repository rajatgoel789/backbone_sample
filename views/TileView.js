window.TileView = Backbone.View.extend({
 
	tagName: 'li', 

	initialize: function() {
	  console.log('TileView initialized');
	},
	render:function () {
	  $(this.el).html(this.template());
	  return this;
	}
 
});
