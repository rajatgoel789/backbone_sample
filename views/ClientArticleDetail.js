window.ClientArticleDetail = Backbone.View.extend({
  
  id: "client-article",

  events:{
     'click dt a.article-remove' : 'deleteArticle',
     'click  .selected-post'     : 'showSelectedPost',
     'click  .tile_like'         : 'userTileLikeDisLike',         
     'click  .tile_dislike'      : 'userTileLikeDisLike',
     'click  .post-share'        : 'OpenShareArticleModal'
    } , 

  initialize: function () {
  	console.log('ClientArticleDetail initialized');
  },

  render: function () {
    $(this.el).html(this.template());
    return this;  
  },

  deleteArticle: function(e) {
     var r=confirm("Are you sure to delete this content ");
    if (r==true){ }else{ return false ; }
    //var articleId = this.model[0]["attributes"].id;
    var articleId = $(e.currentTarget).attr('data-postid');
    var tile = new Tile({ id: articleId, isdelete: 1 });
    var tiles = new Tiles();
    var that = this;
    tile.destroy({
        data: JSON.stringify(tile),
        contentType: 'application/json',
        dataType: "text",
      success: function(response) {
        $(".company").remove();
        $("#loaderdiv").show();
        var companyview = new CompanyView();
       tiles.fetch({
         success: function(response) {
           $(companyview.el).html(companyview.template({tilesCollection: response.toJSON()}));
            $(".col-lg-2").after(companyview.el);
            setTimeout(companyview.loadImages, 2000);

        }
      });
        that.closeModal();
      },
      error: function(error) {
        console.log('error', error);
      }
    });

  },
     showSelectedPost: function (e) {
       var class_check = $(e.currentTarget).attr('class');
        var tileModel = new Tile(); 
        var postId = $(e.currentTarget).attr('data-postid');
        tileModel.set({selected_post : "checked"});
        if(tileModel.get("selected_post") == "checked"){
           var selected_post = tileModel.get('units');
           
             if($.trim(class_check) == "selected-post" && $.cookie('post-cookie') == undefined){

              $(e.currentTarget).addClass("like-active");
              selected_post.push(postId);
              tileModel.set({article_like : "like-active",id : parseInt(postId) });
             var companyview = new CompanyView({model: tileModel});

           }else if($.trim(class_check) == "selected-post" && $.cookie('post-cookie') != undefined){

             var selected_post = $.parseJSON($.cookie('post-cookie'));
              for(var i = 0; i < selected_post.length; i++)  {
                selected_post[i] = parseInt(selected_post[i], 10);
              }
              $(e.currentTarget).addClass("like-active");
              selected_post.push(parseInt(postId));
              tileModel.set({article_like : "like-active",id : parseInt(postId) });
             var companyview = new CompanyView({model: tileModel});
           }else {
              var selected_post = $.parseJSON($.cookie('post-cookie'));
                 for(var i = 0; i < selected_post.length; i++)  {
                    selected_post[i] = parseInt(selected_post[i], 10);
                  } 
                selected_post.splice( $.inArray(postId, selected_post), 1 );
              $(e.currentTarget).removeClass("like-active");
              tileModel.set({article_like : "",id : parseInt(postId) });
             var companyview = new CompanyView({model: tileModel});
           }
              
        console.log("selected_post",JSON.stringify(selected_post));
            $.cookie('post-cookie', JSON.stringify(selected_post));
         }
   },

   OpenShareArticleModal: function (e) {  //alert("company");
      $(".article-share").remove();
      $("#productclk_model").modal('hide');
      $(".modal-backdrop.in").css("opacity", 0);
     // var tiles = new Tiles();
     var tileid= $(e.currentTarget).attr('data-postid'); 
      
       $.ajax({
          url      :  '../article_detail/'+tileid,
         // contentType : "application/json; charset=utf-8",
          dataType    : 'json',
         success: function (response) {
          //alert("dsds");
	         { //alert(tileid); 
      console.log("Complete Response",response);
     
     // var tileModel= response.where({'id':parseInt(tileid)  });
       articleShareView = new ArticleShareView({ collection: response});
      articleShareView.render();
      $("body").append(articleShareView.el);
      $("#share_article").modal('show');
        }
		 
		 
		           },
          error: function (error) {
          console.log("Data not fetched.");
         }
      });
      
        
      
     /* tiles.fetch({
        success: function(response) { //alert(tileid); 
      console.log("Complete Response",response);
     
      var tileModel= response.where({'id':parseInt(tileid)  });
      articleShareView = new ArticleShareView({model: tileModel, collection: tiles});
      articleShareView.render();
      $("body").append(articleShareView.el);
      $("#share_article").modal('show');
        },
        error: function() {
           console.log("Data not fetched.");
        }
      });*/
   },

  userTileLikeDisLike : function(e) {
    console.log('current Modellllllll', this.model);
    //var tileModel = new Tile(); 
    var postId = $(e.currentTarget).attr('data-postid');
    var currentclass=$(e.currentTarget).attr('class'); 
    if (currentclass.toLowerCase().indexOf("tile_like") >= 0) {
        var tl = 1 ;
        var tdl=0 ;
    }else if(currentclass.toLowerCase().indexOf("tile_dislike") >= 0){
  
    var tl = 0 ;
    var tdl=1 ;
    }
    else {
      var tl = 0 ;
      var tdl=0 ;
    }
     var tileDisLike = new Tile({
      tile_id: postId,
      tile_like: tl,
      tile_dislike: tdl,
     });
            
    tileDisLike.save({},{
      success:function(model,response,xhr){
      if(response.error_msg)
      {
        alert(response.error_msg);
        // what if this property exists.
      }
      else
      {
        if (response.like == 1) {
          tileDisLike.set({tile_like :1,tile_dislike :0})
          //alert(tileDisLike.get('tile_like'));
          var tile_like_status =tileDisLike.get('tile_like') ;
          if (tile_like_status==1) {
            $(e.currentTarget).css('pointer-events', 'none');
            $(e.currentTarget).parent().prev().find("a").css('pointer-events', 'auto')
            console.log($(e.currentTarget).parent().prev().find("a"));
            //$(e.currentTarget).parent().siblings("dt .tile_dislike").css('pointer-events', 'none');  
            if ($(e.currentTarget).parent().prev().find("a").hasClass('dislike-active'))
            {
              $(e.currentTarget).parent().prev().find("a").removeClass("dislike-active");
            }
          
            if($(e.currentTarget).hasClass('like-active')){
            }
            else
            {
              $(e.currentTarget).addClass("like-active");
            }

          }
          
        }else if(response.dislike == 1){ //alert("dislike call");
          tileDisLike.set({tile_like :0,tile_dislike :1}) ;
          //  alert(tileLike.get('tile_like'));
          var tile_like_status =tileDisLike.get('tile_like') ;
          if (tile_like_status==0) {
            $(e.currentTarget).css('pointer-events', 'none');
            $(e.currentTarget).parent().next().find("a").css('pointer-events', 'auto');
            if ($(e.currentTarget).parent().next().find("a").hasClass('like-active')) {
              $(e.currentTarget).parent().next().find("a").removeClass("like-active");            
            }
          
            if($(e.currentTarget).hasClass('dislike-active')){
            }
            else
            {
              $(e.currentTarget).addClass("dislike-active");
            }
        
          }
        
        }
        console.log('model value', model);
      var companyview = new CompanyView({ model: tileDisLike });     
      }
      },
      error: function(model,response,xhr){
        alert("error");
      }
    });
  },
  userTileLike : function(e) {
    var tileModel = new Tile(); 
    var postId = $(e.currentTarget).attr('data-postid');
    alert(postId);
    tileModel.set({tile_like : 0,id : parseInt(postId) });
    tileModel.set({tile_dislike : 1,id : parseInt(postId) });
    var companyview = new CompanyView({model: tileModel});
    console.log('current Model', tileModel);
  },

  closeModal : function() {
     $("#productclk_model").modal('hide');
     $(".modal-backdrop.in").css("opacity", "0");    
  }
});