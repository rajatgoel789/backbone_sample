var EmailClients = Backbone.Collection.extend({
  defaults : {
    contacts  : null,
 
  },
  model : EmailClient,

  initialize: function(models,options){
    console.log("Email Client collection initialized");
    console.log("options" , options);
    if(typeof options.view != 'undefined') {
      this.view = options.view;
    }
  },

  url: function() {
  	if(this.view != ""){
      return '../clients_emails/?view='+ this.view;
   }else {
   	return '../clients_emails/';
   }
  },
});

