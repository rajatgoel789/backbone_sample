var Tile = Backbone.Model.extend({
   defaults : {
    id           : null,
    user_id      : "fffa",
    title        : null,
    web_url      : null,
    image_path   : null,
    tile_id      : null ,
    tile_like    : 0,
    tile_dislike : 0,
    selected_post: 'unchecked',
    units        :     []
  },

 url :'../tile_like/',

  initialize: function(){
    console.log("initialize Tile model");
  }  ,

 sync: function(method, model, options) {
    switch (method) {
      case 'create':
        options = options || {};		
			  options.data = JSON.stringify(options.attrs || model.toJSON());
			  Backbone.sync(method, model, options);
        console.log(model); 
        break;

      case 'delete':
        options = options || {};
        console.log("delete options", options);		
			  options.data = JSON.stringify(options.attrs || model.toJSON()); 
			  Backbone.sync(method, model, options);
        break;
  
  }
    
} 
  
});

