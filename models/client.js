var Client = Backbone.Model.extend({
   defaults : {
    id: null,
    client_id : null,
    filter:null,
    next_delivery_date:null,
    auto_delivery_mail:null,
    tile_id      : null ,
    tile_like    : 0,
    tile_dislike : 0,
    selected_post: 'unchecked',
    posts        :     [] ,
    req_type      :null,
  },

  url : '../tile_like/',

  initialize: function(){
    console.log("initialize CLient model");
       console.log(this.get('filter'));
  },

 sync: function(method, model, options) {
    switch (method) {
      case 'create':
        options = options || {};		
			  options.data = JSON.stringify(options.attrs || model.toJSON());
			  Backbone.sync(method, model, options);
        console.log("modelmodelmodelmodelmodelmodel", model); 
        break;

      case 'delete':
        options = options || {};
        console.log("delete options", options);		
			  options.data = JSON.stringify(options.attrs || model.toJSON()); 
			  Backbone.sync(method, model, options);
        break;
  
  }
  
} 
  
});

